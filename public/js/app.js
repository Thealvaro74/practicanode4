
function login() {
    $.ajax({
        url: "/usuarios/login",
        type: "POST",
        data: JSON.stringify({
            login: document.getElementById('login').value,
            password: document.getElementById('password').value
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        beforeSend: (xhr) => {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
        },
        success: function (data) {
            if (data.error) {
                $("#loginError").css('display', 'block');
            } else {
                $("#loginError").css('display', 'none');
                window.location = data.location;
                localStorage.setItem("token", data.token);
            }
        }
    });
}

function nuevo() {
    var formData = new FormData(document.getElementById('formulario'));

    $("#formulario").submit(function (ev) {
        ev.preventDefault();
    });
    $.ajax({
        url: "/inmuebles",
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
        },
        success: function (data) {
            $('#todo').html(data);
        }
    });

}

function eliminar($idImueble) {
    $.ajax(
        {
            url: "/inmuebles/" + $idImueble,
            type: "DELETE",
            data: JSON.stringify({}),
            contentType: "aplication/json; charset=utf-8",
            dataType: "json",
            beforeSend: (xhr) => {
                xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
            },
            success: () => {
                window.location.href = "/inmuebles";
            }
        }
    );
}