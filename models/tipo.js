const mongoose = require('mongoose');

let tipoSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true, //borra espacios a los dos lados
        unique:true //clave unica
    }
});

let Tipo = mongoose.model('tipo', tipoSchema);

module.exports = Tipo;