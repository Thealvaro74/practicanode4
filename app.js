const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const principal = require(__dirname + '/routes/index');
const inmuebles = require(__dirname + '/routes/inmuebles');
const tipos = require(__dirname + '/routes/tipos');
const usuarios = require(__dirname + '/routes/usuarios');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const { Strategy, ExtractJwt } = require('passport-jwt');

const secreto = "secreto";

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/inmuebles');

let app = express();
app.use(passport.initialize());
app.use(fileUpload());

app.set('view engine', 'ejs');
app.use('/public', express.static(__dirname + '/public'));

/*let validarToken =(token)=> {
    try {
        let resultado = jwt.verify(token, secreto);
        return resultado;
    } catch (err) {
        console.log(err);
    }
}*/

passport.use(new Strategy({ secretOrKey: secreto, jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken() }, (payload, done) => {
    if (payload.id) {
        return done(null, { id: payload.id });
    } else {
        return done(new Error("Este usuario es incorrecto"), null);
    }
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/inmuebles', inmuebles);
app.use('/tipos', tipos);
app.use('/usuarios', usuarios);
app.use('/', principal);

app.use((req, res, next) => {
    res.status(404);
    res.render('error', { url: req.url });
});

app.listen(8080);