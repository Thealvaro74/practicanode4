const express = require('express');
let router = express.Router();
let Tipo = require(__dirname + '/../models/tipo');

router.get('/', (req, res) => {
    res.render(__dirname + '/../views/index');
});

router.get('/nuevo_inmueble', (req, res) => {
    Tipo.find().then(result => {
        res.render(__dirname + '/../views/nuevo_inmueble', {
            error: undefined, //paso undefined porque sino cuando lo mando a la vista, si error no contiene nada, no lo coge y me da error
            tipos: result
        });
    }).catch(error => {
        res.render(__dirname + '/../views/nuevo_inmueble', {
            error: undefined,
            tipos: error
        })
    })
});

router.get("/login", (req, res) => {
    res.render(__dirname + '/../views/login', { error: undefined });
});

router.get("/registro", (req, res) => {
    res.render(__dirname + '/../views/registro', { error: undefined });
});

router.get('/prohibido', (req, res) => {
    res.render(__dirname + '/../views/prohibido');
});

router.get('/prohibido_contenido', (req, res) => {
    res.render(__dirname + '/../views/prohibido_contenido');
});

module.exports = router;