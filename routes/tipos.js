const express = require('express');
let Tipo = require('../models/tipo');

let router = express.Router();

router.get('/', (req, res) => {
    Tipo.find().then(resp =>{
        res.send({ tipos: resp });
    }).catch(error =>{
        res.send({
            tipos:[]}
        );
    });
});


module.exports = router;
