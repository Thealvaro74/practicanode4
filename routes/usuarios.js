const express = require('express');
const mongoose = require('mongoose');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
const passport = require('passport');

const Usuario = require(__dirname + '/../models/usuarios');
let Tipo = require(__dirname + '/../models/tipo');
let Inmueble = require(__dirname + '/../models/inmueble.js');

let router = express.Router();

const secreto = "secreto";

let generarToken = id => {
    return jwt.sign({ id: id }, secreto, { expiresIn: "24 hours" });
}

router.post('/registro', (req, res) => {
    let nUsuario = new Usuario({
        nombre: req.body.nombre1,
        login: req.body.login1,
        password: md5(req.body.password1)
    });
    console.log(nUsuario);

    nUsuario.save().then(resultado => {
        res.render("login", { error: undefined });
    }).catch(mensaje => {
        res.render(__dirname + '/../views/registro', { error: true, mensajeError: mensaje });
    });
});

router.post('/login', (req, res) => {
    console.log('req', req);
    Usuario.findOne({ login: req.body.login, password: md5(req.body.password) })
        .then(resultado => {
            if (resultado)
                res.send({ ok: true, token: generarToken(resultado._id), location: '/' });
            else
                res.send({ ok: false, mensajeError: "Los datos no son correctos" });
        }).catch(error => {
            res.send({ ok: false, mensajeError: "No se encuentra el usuario" });
        });
});

module.exports = router;