const express = require('express');
let Inmueble = require(__dirname + '/../models/inmueble');
let Tipo = require(__dirname + '/../models/tipo');
let Usuario = require(__dirname + '/../models/usuarios');
let fs = require('fs');
const mongoose = require('mongoose');
const passport = require('passport');

let router = express.Router();

/******************* Listar todos los inmuebles *******************/
router.get('/', (req, res) => {
    Inmueble.find().populate('tipo').then(resultado => { //hace referencia al campo de la tabla, no a la tabla
        Tipo.find().then(result => {
            //console.log('RESULTADO',resultado);
            //console.log('RES', res);
            res.render(__dirname + '/../views/listar_inmuebles', {
                error: false,
                inmuebles: resultado,
                tipos: result,
                tipo: "undefined"
            });
        }).catch(error => {
            //console.log(error);
            res.render(__dirname + '/../views/listar_inmuebles', {
                error: true,
                inmuebles: resultado,
                tipos: [],
                tipo: "undefined"
            });
        });

    }).catch(error => {
        res.render(__dirname + '/../views/listar_inmuebles', {
            error: true,
            inmuebles: [],
            tipos: [],
            tipo: "undefined"
        });
    });
});

/******************* Listar todos los inmuebles por el tipo *******************/

router.get('/tipo/:id', (req, res) => {
    Inmueble.find({ tipo: req.params.id }).populate('tipo').then(resultado => { //hace referencia al campo de la tabla, no a la tabla
        Tipo.find().then(result => {
            //console.log('RESULTADO',resultado);
            res.render(__dirname + '/../views/listar_inmuebles', {
                error: false,
                inmuebles: resultado,
                tipos: result,
                tipo: req.params.id
            });
        }).catch(error => {
            res.render(__dirname + '/../views/listar_inmuebles', {
                error: true,
                inmuebles: [],
                tipos: [],
                tipo: req.params.id
            });
        });

    }).catch(error => {
        res.render(__dirname + '/../views/listar_inmuebles', {
            error: true,
            inmuebles: [],
            tipos: []
        });
    });
});


/******************* Insertar inmueble *******************/
router.post('/', passport.authenticate('jwt', { session: false, failureRedirect: '/prohibido_contenido' }), (req, res) => {
    let nameImg;

    if (req.files.imagen) {
        let date = new Date();
        nameImg = date.getTime() + '.jpg';

        req.files.imagen.mv('public/uploads/' + nameImg, error => {
            if (error) {
                console.log('No se ha podido subir el archivo', error);
            }
        });
    }
    else {
        nameImg = 'porDefecto.jpg'
    }

    let nuevoInmueble = new Inmueble({
        descripcion: req.body.descripcion,
        tipo: req.body.tipo,
        habitaciones: req.body.habitaciones,
        superficie: req.body.superficie,
        precio: req.body.precio,
        imagen: nameImg
    });

    nuevoInmueble.save().then(resultado => {
        Tipo.find().then(result => {
            res.render(__dirname + '/../views/ficha_inmueble', {
                error: false,
                inmueble: resultado,
                tipos: result
            });
        }).catch(error => {
            res.render(__dirname + '/../views/ficha_inmueble', {
                error: true,
                inmueble: resultado,
                tipos: []
            });
        });
    }).catch(error => {
        console.log("ERROR:", error);
        res.send({
            error: true,
            error2: error,
            mensajeError: "Error al crear el inmueble",
            tipos: []
        });
    });
});

/******************* FILTRADOS *******************/
router.get('/filtro', (req, res) => {

    let fPrecio = req.query.precio;
    let fHabitaciones = req.query.habitaciones;
    let fSuperficie = req.query.superficie;

    console.log('PRECIO', fPrecio);
    console.log('HABITACIONES', fHabitaciones);
    console.log('SUPERFICIE', fSuperficie);


    if (fPrecio >= 0 && fSuperficie >= 0 && fHabitaciones >= 0) {
        let filtrosQuery = {};
        req.query.precio != 0 ? filtrosQuery.precio = { $lte: req.query.precio } : "";
        req.query.habitaciones != 0 ? filtrosQuery.habitaciones = { $gte: req.query.habitaciones } : "";
        req.query.superficie != 0 ? filtrosQuery.superficie = { $gte: req.query.superficie } : "";

        let filtros = {};
        filtros.precio = fPrecio;
        filtros.superficie = fSuperficie;
        filtros.habitaciones = fHabitaciones;

        Inmueble.find(filtrosQuery).populate('tipo').then(resultado => {
            Tipo.find().then(res => {
                //console.log('RESULTADO',resultado);
                res.render(__dirname + '/../views/filtrar_inmuebles', {
                    error: false,
                    inmuebles: resultado,
                    tipos: res,
                    tipo: "undefined",
                    filtros: filtros
                });
            }).catch(error => {
                res.render(__dirname + '/../views/filtrar_inmuebles', {
                    error: false,
                    inmuebles: resultado,
                    tipos: [],
                    tipo: "undefined",
                    filtros: filtros
                });
            });

        }).catch(error => {
            res.render(__dirname + '/../views/filtrar_inmuebles', {
                error: true,
                inmuebles: [],
                tipos: [],
                tipo: "undefined",
                filtros: filtros
            });
        });
    }


});

/******************* Listar inmueble *******************/

router.get('/:id', (req, res) => {
    Inmueble.findById(req.params.id).populate('tipo').then(resultado => {
        Tipo.find().then(result => {
            res.render(__dirname + '/../views/ficha_inmueble', {

                error: false,
                inmueble: resultado,
                tipos: result
            });
        }).catch(error => {
            res.render(__dirname + '/../views/ficha_inmueble', {
                error: true,
                inmueble: resultado,
                tipos: []
            });
        });

    }).catch(error => {
        res.render(__dirname + '/../views/ficha_inmueble', {
            error: true,
            error2: error,
            mensajeError: "Error al buscar inmueble",
            tipos: []
        });
    });
});

/******************* Eliminar inmueble *******************/
router.delete('/:id', passport.authenticate('jwt', { session: false, failureRedirect: '/prohibido_contenido' }), (req, res) => {
    Inmueble.findByIdAndRemove(req.params.id).then(resultado => {

        if (resultado.imagen != 'porDefecto.jpg' && fs.existsSync(__dirname + '/../public/uploads/' + resultado.imagen)) {
            fs.unlink(__dirname + '/../views/public/uploads/' + resultado.imagen, () => null);
        }

        res.send({ error: true, resultado: resultado });

    }).catch(error => {
        res.send({
            error: true,
            resultado: error,
            mensajeError: "Error al borrar el inmueble",
        });
    });
});


module.exports = router;